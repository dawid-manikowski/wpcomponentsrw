package pageObjects;

import main.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;

public class WordpressMailing_Component {

    private static WebDriver driver;

    public WordpressMailing_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void step1() throws InterruptedException {
        logInfo("Trying open e-mail client application.");
        Thread.sleep(1000);
        logPass("Application has been opened successfully.");
    }

    public void step2() throws InterruptedException {
        logInfo("Creating new e-mail message.");
        Thread.sleep(1000);
        logPass("Filling receiver address, subject, message body.");
        Thread.sleep(500);
        logPass("Adding invoice to attachments.");
    }

    public void step3() throws InterruptedException {
        logInfo("Trying to send e-mail.");
        Thread.sleep(1000);
        logPass("E-mail has been send successfully.");
    }

}
