package pageObjects;

import main.BrowserFactory;
import main.Main;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import steps.waits.WaitMethods;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.IOException;

import static extent.reports.ExtentManager.logInfo;
import static utils.PfUtils.getCurrentDateTime;

public class WordpressReports_Component {

    private static WebDriver driver;

    public WordpressReports_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//li[@class='custom ']/form/div/button[@type='submit']")
    private WebElement idzBtn;

    @FindBy(xpath = "//li[@class='custom active']/form/div/button[@type='submit']")
    private WebElement idzBtnActive;

    @FindBy(css = "li[class='custom active']")
    private WebElement customActive;

    @FindBy(xpath = "(//li[@class='custom active']/form/div/input[@name='start_date'])[2]")
    private WebElement startDateInput;

    @FindBy(xpath = "(//li[@class='custom active']/form/div/input[@name='end_date'])[2]")
    private WebElement endDateInput;

    @FindBy(xpath = "//div/a[@class='export_csv']")
    private WebElement exportBtn;

    @FindBy(id = "poststuff")
    private WebElement graphElement;


    /**
     * @param period: year, last_month, month, 7day
     */
    public void selectPeriod(String period){
        if (period.toLowerCase().equals("custom")){

            String startDate = Main.parameters.getParameter("startDate");
            String endDate = Main.parameters.getParameter("endDate");

            if(!startDate.matches("\\d{4}-\\d{2}-\\d{2}") || endDate.matches("\\d{4}-\\d{2}-\\d{2}")){
                throw new IllegalArgumentException("One of data parameters has invalid parameter format. Expected format: YYYY-MM-DD");
            }

            WaitMethods.waitUntilElementIsClickable(idzBtn,15);
            idzBtn.click();
            logInfo("Clicked on 'Idz' button.");
            WaitMethods.waitUntilElementIsVisible(startDateInput,15);
            new Actions(BrowserFactory.getDriver()).click(startDateInput);
            startDateInput.sendKeys(startDate);
            logInfo("Start date of report selected as: " + startDate);
            startDateInput.sendKeys(Keys.TAB);
            endDateInput.sendKeys(endDate);
            logInfo("End date of report selected as: " + endDate);
            idzBtnActive.click();
            logInfo("Clicked on 'Idz' button to generate report.");
        } else {
            WebElement periodBtn = driver.findElement(By.xpath("//div[@class='stats_range']/ul/li/a[contains(@href,'range=" + period + "')]"));
            WaitMethods.waitUntilElementIsClickable(periodBtn,25);
            periodBtn.click();
            logInfo("Selected " + period + " period.");
        }

        WaitMethods.waitForJQueryLoad();
    }

    public void exportCSV(){
        WaitMethods.waitUntilElementIsClickable(exportBtn,20);
        exportBtn.click();
        logInfo("Clicked on 'Export CSV' button.");
    }

    public void getGraphScreenShoot(){
        BrowserFactory.getDriver().manage().window().fullscreen();

        Screenshot fpScreenshot = new AShot().shootingStrategy(ShootingStrategies.viewportRetina(1000, 0, 0, 1.25f)).takeScreenshot(driver);
        String fileName = Main.workspacePath + "result" + System.getProperty("file.separator") + "attachments"
                + System.getProperty("file.separator") + "screenshot_" + getCurrentDateTime("fileName") + ".png";
        try {
            ImageIO.write(fpScreenshot.getImage(), "PNG", new File(fileName));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}
