package pageObjects;

import main.BrowserFactory;
import main.Main;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import steps.waits.WaitMethods;

import static extent.reports.ExtentManager.*;
import static steps.waits.WaitMethods.*;

public class WordpressCoupons_Component {
    private static WebDriver driver;

    public WordpressCoupons_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "coupon_code")
    private WebElement couponInput;

    @FindBy(xpath = "//input[@id='coupon_code']/following-sibling::button")
    private WebElement submitBtn;

    @FindBy(xpath = "//div[contains(text(),'Kupon został pomyślnie użyty')]")
    private WebElement messageSuccess;

    @FindBy(xpath = "//li[contains(text(),'nie istnieje!')]")
    private WebElement messageAlert;


    public void addCoupon(String couponName){
        logInfo("Trying to type coupon name into input field.");
        couponInput.sendKeys(couponName);
        WaitMethods.waitUntilElementIsClickable(submitBtn,10);
        logInfo("Trying to click submit coupon.");
        submitBtn.click();
        logPass("Coupon has been added.");
    }

    public void checkIfCouponAdded() throws InterruptedException {
        int returnValue = WaitMethods.waitUntilOneElementIsDisplayed(messageSuccess, messageAlert, 20);

        if(returnValue == 1){
            logPass("Coupon is valid. Process of adding coupon was successful.");
        } else if (returnValue == 2){
            Thread.sleep(4000);
            throw new AssertionError("Coupon is not valid. Process of adding coupon was unsuccessful.");
        } else {
            throw new AssertionError("Process of adding coupon was unsuccessful.");
        }
    }
}
