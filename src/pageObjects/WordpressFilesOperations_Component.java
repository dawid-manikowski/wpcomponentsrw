package pageObjects;

import main.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;

public class WordpressFilesOperations_Component {

    private static WebDriver driver;

    public WordpressFilesOperations_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void step1() throws InterruptedException {
        logInfo("Trying to find and open the file with orders list.");
        Thread.sleep(1000);
        logPass("Orders file has been found and opened successfully.");
    }

    public void step2() throws InterruptedException {
        logInfo("Selecting today's orders.");
        Thread.sleep(500);
        logPass("Orders for today have been selected.");
    }

    public void step3() throws InterruptedException {
        logInfo("Trying to save and close current file.");
        Thread.sleep(500);
        logPass("File has been saved and closed successfully.");
    }
}
