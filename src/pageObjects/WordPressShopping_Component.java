package pageObjects;

import main.BrowserFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.waits.WaitMethods;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;
import static steps.waits.WaitMethods.waitUntilElementIsNotPresent;
import static steps.waits.WaitMethods.waitUntilElementIsVisible;

public class WordPressShopping_Component {

    private static WebDriver driver;

    public WordPressShopping_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//p[@class='site-title']/a[contains(text(),'RPA demonstration page')]")
    private WebElement siteTitleName;

    @FindBy(xpath = "//p[@class='site-title']/a[contains(text(),'RPA demonstration page')]")
    private WebElement cartPageName;

    @FindBy(xpath = "//a[contains(text(),'Zobacz koszyk')]")
    private WebElement showCartBtn;

    private WebElement getAddToCartBtn(String productName){
        return driver.findElement(By.xpath("//a[contains(text(),'" + productName
                + "')]/ancestor::div[contains(@class,'show-in-grid')]/following-sibling::div[contains(@class,'wc-product__add_to_cart')]"));
    }

    public void waitingForSiteTitle(){
        logInfo("Waiting for shop page to open.");
        WaitMethods.waitUntilElementIsVisible(siteTitleName,30);
        logPass("Shop page has been opened successfully.");
    }

    public void addProductToCart(String productName) throws InterruptedException {
        logInfo("Trying to add " + productName + " to the cart.");
        Thread.sleep(2000);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getAddToCartBtn(productName));
        WaitMethods.waitUntilElementIsVisible(getAddToCartBtn(productName),30);
        getAddToCartBtn(productName).click();
        logPass("Clicked on 'Add to cart' button.");
    }

    public void showCart(){
        logInfo("Trying to click 'Show cart' button.");
        WaitMethods.waitUntilElementIsClickable(showCartBtn,15);
        showCartBtn.click();
        logPass("Clicked on 'Show cart' button.");
    }

    public void checkIfAddingSuccess(){
        try{
            waitUntilElementIsVisible(cartPageName,15);
            logPass("Process of adding product to cart was successful.");
        } catch (NoSuchElementException ex){
            throw new AssertionError("Process of adding product to cart was unsuccessful.");
        }
    }
}