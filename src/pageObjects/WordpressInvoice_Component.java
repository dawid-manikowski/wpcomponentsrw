package pageObjects;

import main.BrowserFactory;
import main.Main;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import steps.waits.WaitMethods;

import java.util.ArrayList;
import java.util.List;

import static extent.reports.ExtentManager.logInfo;
import static extent.reports.ExtentManager.logPass;
import static steps.waits.WaitMethods.waitUntilElementIsClickable;
import static steps.waits.WaitMethods.waitUntilElementIsVisible;

public class WordpressInvoice_Component {

    private static WebDriver driver;
    private static List<String[]> myList =  new ArrayList<>();
    private static List<String> invoicesList = new ArrayList<>();

    public WordpressInvoice_Component(){
        driver = BrowserFactory.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "owner")
    private WebElement sellerBtn;

    @FindBy(id = "inspire_invoices_owner_name")
    private WebElement ownerName;

    @FindBy(id = "inspire_invoices_owner_logo")
    private WebElement ownerLogo;

    @FindBy(id = "inspire_invoices_owner_address")
    private WebElement ownerAddress;

    @FindBy(id = "inspire_invoices_owner_nip")
    private WebElement ownerNIP;

    @FindBy(id = "inspire_invoices_owner_bank_name")
    private WebElement ownerBank;

    @FindBy(id = "inspire_invoices_owner_account_number")
    private WebElement ownerAccount;

    @FindBy(id = "inspire_invoices_client_name")
    private WebElement clientName;

    @FindBy(id = "inspire_invoices_client_street")
    private WebElement clientStreet;

    @FindBy(id = "inspire_invoices_client_postcode")
    private WebElement clientPostcode;

    @FindBy(id = "inspire_invoices_client_city")
    private WebElement clientCity;

    @FindBy(id = "inspire_invoices_client_nip")
    private WebElement clientNIP;

    @FindBy(id = "inspire_invoices_client_country")
    private WebElement clientCountry;

    @FindBy(id = "inspire_invoices_client_phone")
    private WebElement clientPhone;

    @FindBy(id = "inspire_invoices_client_email")
    private WebElement clientEmail;

    @FindBy(xpath = "//button[contains(@class, 'add_product')]")
    private WebElement addProductBtn;

    @FindBy(xpath = "//tr[@class='product_row']/td[1]/input[@type='text']")
    private WebElement productName;

    @FindBy(xpath = "//tr[@class='product_row']/td[2]/input[@type='text']")
    private WebElement PKWiU;

    @FindBy(xpath = "//tr[@class='product_row']/td[3]/input[@type='text']")
    private WebElement jm;

    @FindBy(xpath = "//tr[@class='product_row']/td[4]/input[@type='text']")
    private WebElement ilosc;

    @FindBy(xpath = "//tr[@class='product_row']/td[5]/input[@type='text']")
    private WebElement kwotaNetto;

    @FindBy(xpath = "(//input[@id='publish'])[2]")
    private WebElement publish;

    @FindBy(xpath = "//div[@class='wrap']/a[contains(text(), 'Dodaj now')]")
    private WebElement addNewInvoice;

    @FindBy(id = "title")
    private WebElement invoiceTitle;

    @FindBy(id = "post-search-input")
    private WebElement searchInput;

    @FindBy(id = "search-submit")
    private WebElement searchSubmit;

    @FindBy(xpath = "//td[@data-colname='Akcje']/a[2]")
    private WebElement downloadBtn;

    @FindBy(xpath = "//span[contains(text(),'1 element')]")
    private WebElement oneElementTxt;



    public void addInvoiceTitle(){
        invoicesList.add(invoiceTitle.getAttribute("value"));
    }

    public String getInvoiceTitle(int nr){
        return invoicesList.get(nr);
    }

    public void expandSellerSection(){
        waitUntilElementIsVisible(sellerBtn,15);
        sellerBtn.click();
        logInfo("Expanded 'Seller' section.");
    }

    public void fillOwnerName(String name) throws InterruptedException {
        WaitMethods.waitForJQueryLoad();
        waitUntilElementIsVisible(ownerName,15);
        ownerName.sendKeys(name);
        logInfo("Seller's name typed into input field.");
    }

    public void fillOwnerLogo(String logo){
        ownerLogo.sendKeys(logo);
        logInfo("Seller's logo typed into input field.");
    }

    public void fillOwnerAddress(String address){
        ownerAddress.sendKeys(address);
        logInfo("Seller's address typed into input field.");
    }

    public void fillOwnerNIP(String nip){
        ownerNIP.sendKeys(nip);
        logInfo("Seller's NIP typed into input field.");
    }

    public void fillOwnerBank(String bank){
        ownerBank.sendKeys(bank);
        logInfo("Seller's Bank name typed into input field.");
    }

    public void fillOwnerAccount(String account){
        ownerAccount.sendKeys(account);
        logInfo("Seller's account number typed into input field.");
    }

    public void fillClientName(String name){
        clientName.sendKeys(name);
        logInfo("Client's name typed into input field.");
    }

    public void fillClientStreet(String street){
        clientStreet.sendKeys(street);
        logInfo("Client's street typed into input field.");
    }

    public void fillClientPostcode(String postcode){
        clientPostcode.sendKeys(postcode);
        logInfo("Client's postcode typed into input field.");
    }

    public void fillClientCity(String city){
        clientCity.sendKeys(city);
        logInfo("Client's city typed into input field.");
    }

    public void fillClientNIP(String nip){
        clientNIP.sendKeys(nip);
        logInfo("Client's nip typed into input field.");
    }

    public void fillClientCountry(String country){
        clientCountry.sendKeys(country);
        logInfo("Client's country typed into input field.");
    }

    public void fillClientPhone(String phone){
        clientPhone.sendKeys(phone);
        logInfo("Client's phone typed into input field.");
    }

    public void fillClientEmail(String email){
        clientEmail.sendKeys(email);
        logInfo("Client's email typed into input field.");
    }

    public void clickAddProduct(){
        addProductBtn.click();
        logInfo("Clicked on 'Add product' button.");
    }

    public void fillProductName(String name){
        productName.sendKeys(name);
        logInfo("Product's name typed into input field.");
    }

    public void fillPKWiU(String pkw){
        PKWiU.sendKeys(pkw);
        logInfo("Product's PKWiU typed into input field.");
    }

    public void fillJM(String sJM){
        jm.sendKeys(sJM);
        logInfo("Product j.m. typed into input field.");
    }

    public void fillAmount(String amount){
        ilosc.sendKeys(amount);
        logInfo("Product amount typed into input field.");
    }

    public void fillNetto(String netto){
        kwotaNetto.sendKeys(netto);
        logInfo("Product's netto value typed into input field.");
    }

    public void publishInvoice() {
        new Actions(driver).moveToElement(publish).perform();
        waitUntilElementIsVisible(publish,15);
        publish.click();
        logInfo("Clicked on publish button.");
    }

    public void addNextInvoice() {
        new Actions(driver).moveToElement(addNewInvoice).perform();
        waitUntilElementIsVisible(addNewInvoice,15);
        addNewInvoice.click();
        logInfo("Clicked on 'Add new invoice' button.");
    }

    public void readClientsData(){
        String clients = Main.parameters.getParameter("KLIENCI");

        String[] splitString = clients.split("\\r\\n");
        int splitStringLength = splitString.length;

        //wyciagamy tylko 2 klientow -> tyle faktycznie wprowadza megafakturka
        for(int i = (splitStringLength - 1); i > (splitStringLength - 3); i--){
            myList.add(splitString[i].split("\\t"));
        }
    }

    public String[] getClientRow(int row){
        return myList.get(row);
    }

    public void fillAllClientsData(String[] array){
            fillClientName(array[1]);
            fillClientStreet(array[2]);
            fillClientPostcode(array[3]);
            fillClientCity(array[4]);
            fillClientNIP(array[5]);
            fillClientCountry("Polska");
            fillClientPhone(array[7]);
            fillClientEmail(array[8]);
    }

    public void findInvoice(String name){
        waitUntilElementIsVisible(searchInput,30);
        searchInput.clear();
        searchInput.sendKeys(name);
        logInfo("Typed invoice name into search form: " + name + ".");
        waitUntilElementIsClickable(searchSubmit,15);
        searchSubmit.click();
        logInfo("Clicked 'search' button.");
        waitUntilElementIsVisible(oneElementTxt,15);
        waitUntilElementIsVisible(driver.findElement(By.xpath("//tbody/tr[1]/td/strong/a[contains(text(),'" + name + "')]")),15);
    }

    public void downloadInvoice(String name){
        downloadBtn.click();
        logInfo("Clicked 'download invoice' button.");
        waitUntilElementIsVisible(oneElementTxt,15);
        waitUntilElementIsVisible(driver.findElement(By.xpath("//tbody/tr[1]/td/strong/a[contains(text(),'" + name + "')]")),15);
    }

    public void checkIfOneTabOpened(){
        while (driver.getWindowHandles().size() > 1){
            //just wait
        }
    }

    public void step1() throws InterruptedException {
        logInfo("Trying to find and open directory with invoices.");
        Thread.sleep(1000);
        logPass("Invoice directory has been found and opened successfully.");
    }

    public void step2() throws InterruptedException {
        logInfo("Looking for invoices for which the payment deadline have expired.");
        Thread.sleep(700);
        logPass("Invoices after payment deadline was found.");
    }

}